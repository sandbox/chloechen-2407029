
-- SUMMARY --

The Owl Carousel Field Collection module provides a field formatter to render 
field collection entities in a Carousel.


-- REQUIREMENTS --

* Owl Carousel - https://www.drupal.org/project/owlcarousel
* Field Collection - http://drupal.org/project/field_collection



-- INSTALLATION --

* Install as usual, see https://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* To use Carousel Field Collection in one of your content types, you need to 
  add a multi-value field collection field to the content type and set the 
  display formatter of that field to Owl Carousel Collection.

* Owl Carousel settings can be found at admin/config/user-interface/owlcarousel.
* Once you have configured your settings, select the desired settings group 
* for your field collection.
* See http://legomenon.io/article/drupal-7-configuration-owl-carousel 
* for instructions with images =).

  
-- CONTACT --

Chloe Chen  
Morpht.com
